﻿cmake_minimum_required (VERSION 3.26)
project (AoE2DECore)

# Require C++20 support
set (CMAKE_CXX_STANDARD 20)
set (CMAKE_CXX_STANDARD_REQUIRED True)

# Configure stb_image_write
add_subdirectory (vendor/stb_image_write-1.16)

# Library source
add_subdirectory (source)

# Library tools
add_subdirectory (tools)