// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Core/Media/SLDTexture.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <stb/stb_image_write.h>

// --------------------------------------------------------------------------------- System Headers
#include <filesystem>
#include <format>
#include <iostream>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cerr << "Usage: slddump <file> <directory>" << std::endl;
        std::cerr << "<file>        The path to the source .sld file to extract" << std::endl;
        std::cerr << "<directory>   The path to the output directory" << std::endl;
        return 1;
    }

    if (!std::filesystem::is_regular_file(argv[1]))
    {
        std::cerr << "Bad source file path" << std::endl;
        return 1;
    }

    if (!std::filesystem::is_directory(argv[2]))
    {
        std::cerr << "Bad output directory path" << std::endl;
        return 1;
    }

    // Load unit files with 16 directions.
    AoE2DE::Core::Media::SLDTexture lTexture;
    AoE2DE::Core::Media::SLDTexture::LoadOptions lUnitLoadOptions{
        .Directions{ 16UL },
        .CopyMode{ AoE2DE::Core::Media::SLDTexture::LoadOptions::CopyModeOptions::PreviousFrame }
    };
    if (lTexture.LoadFromFile(std::filesystem::path{ argv[1] }, lUnitLoadOptions))
    {
        const std::filesystem::path lOutputBasePath{ argv[2] };
        for (std::size_t lFrameIndex{}; lFrameIndex < lTexture.GetFrameCount(); lFrameIndex++)
        {
            const auto& lColorLayer{ lTexture.GetLayer(lFrameIndex, AoE2DE::Core::Media::SLDTexture::LayerType::Color) };
            if (lColorLayer.Texels)
            {
                stbi_write_png((lOutputBasePath / std::format("color_layer_{}.png", lFrameIndex)).generic_string().c_str(),
                    lColorLayer.Width, lColorLayer.Height, 4, lColorLayer.Texels, lColorLayer.Width * 4);
            }

            const auto& lShadowLayer{ lTexture.GetLayer(lFrameIndex, AoE2DE::Core::Media::SLDTexture::LayerType::Shadow) };
            if (lShadowLayer.Texels)
            {
                stbi_write_png((lOutputBasePath / std::format("shadow_layer_{}.png", lFrameIndex)).generic_string().c_str(),
                    lShadowLayer.Width, lShadowLayer.Height, 1, lShadowLayer.Texels, lShadowLayer.Width);
            }

            const auto& lPlayerColorMask{ lTexture.GetLayer(lFrameIndex, AoE2DE::Core::Media::SLDTexture::LayerType::PlayerColorMask) };
            if (lPlayerColorMask.Texels)
            {
                stbi_write_png((lOutputBasePath / std::format("player_layer_{}.png", lFrameIndex)).generic_string().c_str(),
                    lPlayerColorMask.Width, lPlayerColorMask.Height, 1, lPlayerColorMask.Texels, lPlayerColorMask.Width);
            }

            const auto& lDamageColorMask{ lTexture.GetLayer(lFrameIndex, AoE2DE::Core::Media::SLDTexture::LayerType::DamageColorMask) };
            if (lDamageColorMask.Texels)
            {
                stbi_write_png((lOutputBasePath / std::format("damage_layer_{}.png", lFrameIndex)).generic_string().c_str(),
                    lDamageColorMask.Width, lDamageColorMask.Height, 4, lDamageColorMask.Texels, lDamageColorMask.Width * 4);
            }
        }
    }
    else
    {
        std::cerr << "Failed to decode .sld file" << std::endl;
        return 1;
    }
}