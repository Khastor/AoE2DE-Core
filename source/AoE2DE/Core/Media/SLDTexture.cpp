///////////////////////////////////////////////////////////////////////////////////////////////////
// SLDTexture Implementation                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <AoE2DE/Core/Media/SLDTexture.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>

namespace AoE2DE::Core::Media
{

namespace
{

enum SLDLayerType : std::uint8_t
{
    SLDFrameColorLayerBit                       = 1 << 0,
    SLDFrameShadowLayerBit                      = 1 << 1,
    SLDFrameUnknownLayer1Bit                    = 1 << 2,
    SLDFrameDamageMaskLayerBit                  = 1 << 3,
    SLDFramePlayerMaskLayerBit                  = 1 << 4,
    SLDFrameUnknownLayer2Bit                    = 1 << 5,
    SLDFrameUnknownLayer3Bit                    = 1 << 6,
    SLDFrameUnknownLayer4Bit                    = 1 << 7
};

enum SLDColorLayerHeaderHint : std::uint8_t
{
    SLDColorLayerUnknownHint1Bit                = 1 << 0,
    SLDColorLayerUnknownHint2Bit                = 1 << 1,
    SLDColorLayerUnknownHint3Bit                = 1 << 2,
    SLDColorLayerUnknownHint4Bit                = 1 << 3,
    SLDColorLayerUnknownHint5Bit                = 1 << 4,
    SLDColorLayerUnknownHint6Bit                = 1 << 5,
    SLDColorLayerUnknownHint7Bit                = 1 << 6,
    SLDColorLayerCopySourceLayerBit             = 1 << 7
};

enum SLDDamageMaskLayerHeaderHint : std::uint8_t
{
    SLDDamageMaskLayerUnknownHint1Bit           = 1 << 0,
    SLDDamageMaskLayerUnknownHint2Bit           = 1 << 1,
    SLDDamageMaskLayerUnknownHint3Bit           = 1 << 2,
    SLDDamageMaskLayerUnknownHint4Bit           = 1 << 3,
    SLDDamageMaskLayerUnknownHint5Bit           = 1 << 4,
    SLDDamageMaskLayerUnknownHint6Bit           = 1 << 5,
    SLDDamageMaskLayerUnknownHint7Bit           = 1 << 6,
    SLDDamageMaskLayerCopySourceLayerBit        = 1 << 7
};

enum SLDPlayerMaskLayerHeaderHint : std::uint8_t
{
    SLDPlayerMaskLayerUnknownHint1Bit           = 1 << 0,
    SLDPlayerMaskLayerUnknownHint2Bit           = 1 << 1,
    SLDPlayerMaskLayerUnknownHint3Bit           = 1 << 2,
    SLDPlayerMaskLayerUnknownHint4Bit           = 1 << 3,
    SLDPlayerMaskLayerUnknownHint5Bit           = 1 << 4,
    SLDPlayerMaskLayerUnknownHint6Bit           = 1 << 5,
    SLDPlayerMaskLayerUnknownHint7Bit           = 1 << 6,
    SLDPlayerMaskLayerCopySourceLayerBit        = 1 << 7
};

enum SLDShadowLayerHeaderHint : std::uint8_t
{
    SLDShadowLayerUnknownHint1Bit               = 1 << 0,
    SLDShadowLayerUnknownHint2Bit               = 1 << 1,
    SLDShadowLayerUnknownHint3Bit               = 1 << 2,
    SLDShadowLayerUnknownHint4Bit               = 1 << 3,
    SLDShadowLayerUnknownHint5Bit               = 1 << 4,
    SLDShadowLayerUnknownHint6Bit               = 1 << 5,
    SLDShadowLayerUnknownHint7Bit               = 1 << 6,
    SLDShadowLayerCopySourceLayerBit            = 1 << 7
};

/**
 * @brief SLD file header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#header.
 */
struct SLDTextureHeader
{
    char Descriptor[4];
    std::uint16_t Version;
    std::uint16_t Frames;
    std::uint16_t Unknown1;
    std::uint16_t Unknown2;
    std::uint32_t Unknown3;
};

/**
 * @brief SLD frame header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#sld-frame.
 */
struct SLDFrameHeader
{
    std::uint16_t Width;
    std::uint16_t Height;
    std::int16_t HotspotX;
    std::int16_t HotspotY;
    std::uint8_t Layers;
    std::uint8_t Unknown1;
    std::uint16_t Index;
};

/**
 * @brief SLD layer header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#layer-content-length.
 */
struct SLDLayerHeader
{
    std::uint32_t Length;
    std::uint8_t Padding;
};

/**
 * @brief SLD color layer header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#sld-main-graphics-layer.
 */
struct SLDLayerHeaderColorDetails
{
    std::uint16_t OffsetX1;
    std::uint16_t OffsetY1;
    std::uint16_t OffsetX2;
    std::uint16_t OffsetY2;
    std::uint8_t Hints;
    std::uint8_t Unknown1;
};

/**
 * @brief SLD damage mask layer header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#sld-damage-mask-layer.
 */
struct SLDLayerHeaderDamageMaskDetails
{
    std::uint8_t Hints;
    std::uint8_t Unknown1;
};

/**
 * @brief SLD player mask layer header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#sld-damage-mask-layer.
 */
struct SLDLayerHeaderPlayerMaskDetails
{
    std::uint8_t Hints;
    std::uint8_t Unknown1;
};

/**
 * @brief SLD shadow layer header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#sld-shadow-graphics-layer.
 */
struct SLDLayerHeaderShadowDetails
{
    std::uint16_t OffsetX1;
    std::uint16_t OffsetY1;
    std::uint16_t OffsetX2;
    std::uint16_t OffsetY2;
    std::uint8_t Hints;
    std::uint8_t Unknown1;
};

/**
 * @brief SLD command array header.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#command-array.
 */
struct SLDCommandArrayHeader
{
    std::uint16_t Length;
};

/**
 * @brief SLD draw command.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#command-array.
 */
struct SLDCommand
{
    std::uint8_t SkipCount;
    std::uint8_t DrawCount;
};

/**
 * @brief SLD texel block with BC1 compression.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#excursion-dxt1bc1-block-compression.
 */
struct SLDBC1Block
{
    std::uint16_t Color0;
    std::uint16_t Color1;
    std::uint32_t Indices;
};

/**
 * @brief SLD texel block with BC4 compression.
 *
 * @see https://github.com/SFTtech/openage/blob/master/doc/media/sld-files.md#excursion-dxt4bc4-block-compression.
 */
struct SLDBC4Block
{
    std::uint8_t Color0;
    std::uint8_t Color1;
    std::uint8_t Indices[6];
};

std::istream& operator>>(
    std::istream& pStream,
    SLDTextureHeader& pHeader)
{
    pStream.read(reinterpret_cast<char*>(&pHeader.Descriptor), sizeof(pHeader.Descriptor));
    pStream.read(reinterpret_cast<char*>(&pHeader.Version), sizeof(pHeader.Version));
    pStream.read(reinterpret_cast<char*>(&pHeader.Frames), sizeof(pHeader.Frames));
    pStream.read(reinterpret_cast<char*>(&pHeader.Unknown1), sizeof(pHeader.Unknown1));
    pStream.read(reinterpret_cast<char*>(&pHeader.Unknown2), sizeof(pHeader.Unknown2));
    pStream.read(reinterpret_cast<char*>(&pHeader.Unknown3), sizeof(pHeader.Unknown3));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDFrameHeader& pFrameHeader)
{
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.Width), sizeof(pFrameHeader.Width));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.Height), sizeof(pFrameHeader.Height));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.HotspotX), sizeof(pFrameHeader.HotspotX));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.HotspotY), sizeof(pFrameHeader.HotspotY));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.Layers), sizeof(pFrameHeader.Layers));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.Unknown1), sizeof(pFrameHeader.Unknown1));
    pStream.read(reinterpret_cast<char*>(&pFrameHeader.Index), sizeof(pFrameHeader.Index));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDLayerHeader& pLayerHeader)
{
    pStream.read(reinterpret_cast<char*>(&pLayerHeader.Length), sizeof(pLayerHeader.Length));
    pLayerHeader.Padding = (4 - (pLayerHeader.Length % 4)) % 4;
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDLayerHeaderColorDetails& pLayerHeaderColorDetails)
{
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.OffsetX1), sizeof(pLayerHeaderColorDetails.OffsetX1));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.OffsetY1), sizeof(pLayerHeaderColorDetails.OffsetY1));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.OffsetX2), sizeof(pLayerHeaderColorDetails.OffsetX2));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.OffsetY2), sizeof(pLayerHeaderColorDetails.OffsetY2));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.Hints), sizeof(pLayerHeaderColorDetails.Hints));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderColorDetails.Unknown1), sizeof(pLayerHeaderColorDetails.Unknown1));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDLayerHeaderDamageMaskDetails& pLayerHeaderDamageMaskDetails)
{
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderDamageMaskDetails.Hints), sizeof(pLayerHeaderDamageMaskDetails.Hints));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderDamageMaskDetails.Unknown1), sizeof(pLayerHeaderDamageMaskDetails.Unknown1));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDLayerHeaderPlayerMaskDetails& pLayerHeaderPlayerMaskDetails)
{
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderPlayerMaskDetails.Hints), sizeof(pLayerHeaderPlayerMaskDetails.Hints));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderPlayerMaskDetails.Unknown1), sizeof(pLayerHeaderPlayerMaskDetails.Unknown1));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDLayerHeaderShadowDetails& pLayerHeaderShadowDetails)
{
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.OffsetX1), sizeof(pLayerHeaderShadowDetails.OffsetX1));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.OffsetY1), sizeof(pLayerHeaderShadowDetails.OffsetY1));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.OffsetX2), sizeof(pLayerHeaderShadowDetails.OffsetX2));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.OffsetY2), sizeof(pLayerHeaderShadowDetails.OffsetY2));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.Hints), sizeof(pLayerHeaderShadowDetails.Hints));
    pStream.read(reinterpret_cast<char*>(&pLayerHeaderShadowDetails.Unknown1), sizeof(pLayerHeaderShadowDetails.Unknown1));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDCommandArrayHeader& pCommandArrayHeader)
{
    pStream.read(reinterpret_cast<char*>(&pCommandArrayHeader.Length), sizeof(pCommandArrayHeader.Length));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDCommand& pCommand)
{
    pStream.read(reinterpret_cast<char*>(&pCommand.SkipCount), sizeof(pCommand.SkipCount));
    pStream.read(reinterpret_cast<char*>(&pCommand.DrawCount), sizeof(pCommand.DrawCount));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDBC1Block& pBC1Block)
{
    pStream.read(reinterpret_cast<char*>(&pBC1Block.Color0), sizeof(pBC1Block.Color0));
    pStream.read(reinterpret_cast<char*>(&pBC1Block.Color1), sizeof(pBC1Block.Color1));
    pStream.read(reinterpret_cast<char*>(&pBC1Block.Indices), sizeof(pBC1Block.Indices));
    return pStream;
}

std::istream& operator>>(
    std::istream& pStream,
    SLDBC4Block& pBC4Block)
{
    pStream.read(reinterpret_cast<char*>(&pBC4Block.Color0), sizeof(pBC4Block.Color0));
    pStream.read(reinterpret_cast<char*>(&pBC4Block.Color1), sizeof(pBC4Block.Color1));
    pStream.read(reinterpret_cast<char*>(&pBC4Block.Indices), sizeof(pBC4Block.Indices));
    return pStream;
}

/**
 * @brief 32-bit RGBA color.
 */
struct Color
{
    /**
     * @brief Create a RGBA32 color from a RGB16 color.
     */
    static Color FromRGB16(
        std::uint16_t pColor,
        std::uint8_t pAlpha)
    {
        return Color{
            .R = static_cast<std::uint8_t>((pColor & 0xF800) >> 8),
            .G = static_cast<std::uint8_t>((pColor & 0x07E0) >> 3),
            .B = static_cast<std::uint8_t>((pColor & 0x001F) << 3),
            .A = pAlpha
        };
    }

    /**
     * @brief Linear interpolation of two colors.
     */
    static Color Lerp(
        const Color& pLHS,
        const Color& pRHS,
        float pFactor)
    {
        return Color{
            .R = static_cast<std::uint8_t>(0.5f + pLHS.R + pFactor * (static_cast<int>(pRHS.R) - static_cast<int>(pLHS.R))),
            .G = static_cast<std::uint8_t>(0.5f + pLHS.G + pFactor * (static_cast<int>(pRHS.G) - static_cast<int>(pLHS.G))),
            .B = static_cast<std::uint8_t>(0.5f + pLHS.B + pFactor * (static_cast<int>(pRHS.B) - static_cast<int>(pLHS.B))),
            .A = static_cast<std::uint8_t>(0.5f + pLHS.A + pFactor * (static_cast<int>(pRHS.A) - static_cast<int>(pLHS.A)))
        };
    }

    std::uint8_t R;
    std::uint8_t G;
    std::uint8_t B;
    std::uint8_t A;
};

/**
 * @brief Decode BC1 texel data into the target texture layer.
 *
 * The source layer can be null if the copy layer hint is not set, otherwise it is mandatory.
 *
 * @param pTargetLayer The layer to fill with texel data decoded from the BC1 commands and blocks.
 * @param pSourceLayer The layer to copy texel data from to fill the skipped blocks in the target layer.
 * @param pCommands The BC1 decoding commands.
 * @param pBlocks The BC1 blocks to decode.
 * @param pCopyLayerHint True to copy texel data from the source layer to fill the skipped blocks in the target layer.
 * @return True if the decoding was successfull.
 */
bool DecodeBC1Layer(
    SLDTexture::Layer<true>& pTargetLayer,
    SLDTexture::Layer<true>& pSourceLayer,
    const std::vector<SLDCommand>& pCommands,
    const std::vector<SLDBC1Block>& pBlocks,
    bool pCopyLayerHint)
{
    const std::uint32_t lLayerWidthTexel{ pTargetLayer.Width };
    const std::uint32_t lLayerHeightTexel{ pTargetLayer.Height };
    const std::uint32_t lLayerWidthBlock{ lLayerWidthTexel / 4 };

    std::uint16_t lSourceBlockIndex{};
    std::uint16_t lTargetBlockIndex{};
    for (const SLDCommand& lCommand : pCommands)
    {
        // Process the blocks to skip: either copy from the source layer or fill with null data.
        if (pCopyLayerHint)
        {
            if (!pSourceLayer.Texels)
            {
                return false;
            }

            const std::uint16_t lEndBlockIndex{ static_cast<std::uint16_t>(lTargetBlockIndex + lCommand.SkipCount) };
            for (; lTargetBlockIndex < lEndBlockIndex; lTargetBlockIndex++)
            {
                const std::uint32_t lTargetBlockX{ lTargetBlockIndex % lLayerWidthBlock };
                const std::uint32_t lTargetBlockY{ lTargetBlockIndex / lLayerWidthBlock };

                // Position of the first texel relative to the canvas origin.
                const std::int32_t lCanvasTexelX{ static_cast<std::int32_t>(lTargetBlockX * 4 + pTargetLayer.OffsetX) };
                const std::int32_t lCanvasTexelY{ static_cast<std::int32_t>(lTargetBlockY * 4 + pTargetLayer.OffsetY) };

                // Position of the first texel relative to the source layer origin.
                const std::int32_t lSourceTexelX = lCanvasTexelX - pSourceLayer.OffsetX;
                const std::int32_t lSourceTexelY = lCanvasTexelY - pSourceLayer.OffsetY;

                // Copy the source layer into the target layer if the block is within the bounds of the source layer.
                if (lCanvasTexelX < static_cast<std::int32_t>(pSourceLayer.OffsetX)
                    || lCanvasTexelX > static_cast<std::int32_t>(pSourceLayer.OffsetX + pSourceLayer.Width - 4)
                    || lCanvasTexelY < static_cast<std::int32_t>(pSourceLayer.OffsetY)
                    || lCanvasTexelY > static_cast<std::int32_t>(pSourceLayer.OffsetY + pSourceLayer.Height - 4))
                {
                    // Initialize the target head to the first byte of the current block to draw.
                    const std::uint32_t lTargetHeadIndex{ static_cast<std::uint32_t>(lLayerWidthBlock * lTargetBlockY * 64 + lTargetBlockX * 16) };
                    const std::uint32_t lTargetPitch{ 16 * lLayerWidthBlock };
                    for (std::uint32_t lRowIndex{}; lRowIndex < 4; lRowIndex++)
                    {
                        std::memset(pTargetLayer.Texels + lTargetHeadIndex + lRowIndex * lTargetPitch, 0, 16);
                    }
                }
                else
                {
                    // Initialize the target head to the first byte of the current block to draw.
                    const std::uint32_t lTargetHeadIndex{ static_cast<std::uint32_t>(lLayerWidthBlock * lTargetBlockY * 64 + lTargetBlockX * 16) };
                    const std::uint32_t lSourceHeadIndex{ static_cast<std::uint32_t>(lSourceTexelY * pSourceLayer.Width * 4 + lSourceTexelX * 4) };
                    const std::uint32_t lTargetPitch{ static_cast < std::uint32_t>(16 * lLayerWidthBlock) };
                    const std::uint32_t lSourcePitch{ static_cast < std::uint32_t>(4 * pSourceLayer.Width) };
                    for (std::uint32_t lRowIndex{}; lRowIndex < 4; lRowIndex++)
                    {
                        std::memcpy(
                            pTargetLayer.Texels + lTargetHeadIndex + lRowIndex * lTargetPitch,
                            pSourceLayer.Texels + lSourceHeadIndex + lRowIndex * lSourcePitch, 16);
                    }
                }
            }
        }
        else
        {
            lTargetBlockIndex += lCommand.SkipCount;
        }

        // Process the blocks to render.
        const std::uint16_t lEndBlockIndex{ static_cast<std::uint16_t>(lTargetBlockIndex + lCommand.DrawCount) };
        for (; lTargetBlockIndex < lEndBlockIndex; lTargetBlockIndex++)
        {
            const SLDBC1Block& lBlock{ pBlocks[lSourceBlockIndex++] };

            Color lColors[4]{ Color::FromRGB16(lBlock.Color0, 0xFF), Color::FromRGB16(lBlock.Color1, 0xFF) };
            if (lBlock.Color0 > lBlock.Color1)
            {
                lColors[2] = Color::Lerp(lColors[0], lColors[1], 1.0 / 3.0);
                lColors[3] = Color::Lerp(lColors[0], lColors[1], 2.0 / 3.0);
            }
            else
            {
                lColors[2] = Color::Lerp(lColors[0], lColors[1], 0.5);
                lColors[3] = Color::FromRGB16(0, 0);
            }

            const std::uint32_t lTargetBlockX{ lTargetBlockIndex % lLayerWidthBlock };
            const std::uint32_t lTargetBlockY{ lTargetBlockIndex / lLayerWidthBlock };

            // Initialize the head to the first byte of the current block to draw.
            std::uint32_t lHeadIndex{ lLayerWidthBlock * lTargetBlockY * 64 + lTargetBlockX * 16 };

            std::uint32_t lLookUpIndices{ lBlock.Indices };
            for (std::uint32_t lTexelIndex{}; lTexelIndex < 16; lTexelIndex++)
            {
                // Move the head to the next row.
                if (lTexelIndex % 4 == 0 && lTexelIndex != 0)
                {
                    lHeadIndex += 16 * (lLayerWidthBlock - 1);
                }

                std::uint32_t lLookUpIndex = lLookUpIndices & 0x3;
                pTargetLayer.Texels[lHeadIndex++] = lColors[lLookUpIndex].R;
                pTargetLayer.Texels[lHeadIndex++] = lColors[lLookUpIndex].G;
                pTargetLayer.Texels[lHeadIndex++] = lColors[lLookUpIndex].B;
                pTargetLayer.Texels[lHeadIndex++] = lColors[lLookUpIndex].A;
                lLookUpIndices >>= 2;
            }
        }
    }
    return true;
}

/**
 * @brief Decode BC4 texel data into the target texture layer.
 *
 * The source layer can be null if the copy layer hint is not set, otherwise it is mandatory.
 *
 * @param pTargetLayer The layer to fill with texel data decoded from the BC4 commands and blocks.
 * @param pSourceLayer The layer to copy texel data from to fill the skipped blocks in the target layer.
 * @param pCommands The BC4 decoding commands.
 * @param pBlocks The BC4 blocks to decode.
 * @param pCopyLayerHint True to copy texel data from the source layer to fill the skipped blocks in the target layer.
 * @return True if the decoding was successfull.
 */
bool DecodeBC4Layer(
    SLDTexture::Layer<true>& pTargetLayer,
    SLDTexture::Layer<true>& pSourceLayer,
    const std::vector<SLDCommand>& pCommands,
    const std::vector<SLDBC4Block>& pBlocks,
    bool pCopyLayerHint)
{
    const std::uint32_t lLayerWidthTexel{ pTargetLayer.Width };
    const std::uint32_t lLayerHeightTexel{ pTargetLayer.Height };
    const std::uint32_t lLayerWidthBlock{ lLayerWidthTexel / 4 };

    std::uint16_t lSourceBlockIndex{};
    std::uint16_t lTargetBlockIndex{};
    for (const SLDCommand& lCommand : pCommands)
    {
        // Process the blocks to skip: either copy from the source layer or fill with null data.
        if (pCopyLayerHint)
        {
            if (!pSourceLayer.Texels)
            {
                return false;
            }

            const std::uint16_t lEndBlockIndex{ static_cast<std::uint16_t>(lTargetBlockIndex + lCommand.SkipCount) };
            for (; lTargetBlockIndex < lEndBlockIndex; lTargetBlockIndex++)
            {
                const std::uint32_t lTargetBlockX{ lTargetBlockIndex % lLayerWidthBlock };
                const std::uint32_t lTargetBlockY{ lTargetBlockIndex / lLayerWidthBlock };

                // Position of the first texel relative to the canvas origin.
                const std::int32_t lCanvasTexelX{ static_cast<std::int32_t>(lTargetBlockX * 4 + pTargetLayer.OffsetX) };
                const std::int32_t lCanvasTexelY{ static_cast<std::int32_t>(lTargetBlockY * 4 + pTargetLayer.OffsetY) };

                // Position of the first texel relative to the source layer origin.
                const std::int32_t lSourceTexelX = lCanvasTexelX - pSourceLayer.OffsetX;
                const std::int32_t lSourceTexelY = lCanvasTexelY - pSourceLayer.OffsetY;

                // Copy the source layer into the target layer if the block is within the bounds of the source layer.
                if (lCanvasTexelX < static_cast<std::int32_t>(pSourceLayer.OffsetX)
                    || lCanvasTexelX > static_cast<std::int32_t>(pSourceLayer.OffsetX + pSourceLayer.Width - 4)
                    || lCanvasTexelY < static_cast<std::int32_t>(pSourceLayer.OffsetY)
                    || lCanvasTexelY > static_cast<std::int32_t>(pSourceLayer.OffsetY + pSourceLayer.Height - 4))
                {
                    // Initialize the target head to the first byte of the current block to draw.
                    const std::uint32_t lTargetHeadIndex{ static_cast<std::uint32_t>(lLayerWidthBlock * lTargetBlockY * 16 + lTargetBlockX * 4) };
                    const std::uint32_t lTargetPitch{ 4 * lLayerWidthBlock };
                    for (std::uint32_t lRowIndex{}; lRowIndex < 4; lRowIndex++)
                    {
                        std::memset(pTargetLayer.Texels + lTargetHeadIndex + lRowIndex * lTargetPitch, 0, 4);
                    }
                }
                else
                {
                    // Initialize the target head to the first byte of the current block to draw.
                    const std::int32_t lTargetHeadIndex{ static_cast<std::int32_t>(lLayerWidthBlock * lTargetBlockY * 16 + lTargetBlockX * 4) };
                    const std::int32_t lSourceHeadIndex{ static_cast<std::int32_t>(lSourceTexelY * pSourceLayer.Width + lSourceTexelX) };
                    const std::uint32_t lTargetPitch{ 4 * lLayerWidthBlock };
                    const std::uint32_t lSourcePitch{ pSourceLayer.Width };

                    for (std::uint32_t lRowIndex{}; lRowIndex < 4; lRowIndex++)
                    {
                        std::memcpy(
                            pTargetLayer.Texels + lTargetHeadIndex + lRowIndex * lTargetPitch,
                            pSourceLayer.Texels + lSourceHeadIndex + lRowIndex * lSourcePitch, 4);
                    }
                }
            }
        }
        else
        {
            lTargetBlockIndex += lCommand.SkipCount;
        }

        // Process the blocks to render.
        const std::uint16_t lEndBlockIndex{ static_cast<std::uint16_t>(lTargetBlockIndex + lCommand.DrawCount) };
        for (; lTargetBlockIndex < lEndBlockIndex; lTargetBlockIndex++)
        {
            const SLDBC4Block& lBlock{ pBlocks[lSourceBlockIndex++] };

            std::uint8_t lColors[8]{ lBlock.Color0, lBlock.Color1, 0, 0, 0, 0, 0, 255 };
            if (lBlock.Color0 > lBlock.Color1)
            {
                for (std::size_t lIndex{ 1ULL }; lIndex <= 6ULL; lIndex++)
                {
                    lColors[lIndex + 1ULL] = (lIndex * lBlock.Color1 + (7ULL - lIndex) * lBlock.Color0) / 7;
                }
            }
            else
            {
                for (std::size_t lIndex{ 1ULL }; lIndex <= 4ULL; lIndex++)
                {
                    lColors[lIndex + 1ULL] = (lIndex * lBlock.Color1 + (5ULL - lIndex) * lBlock.Color0) / 5;
                }
            }

            const std::uint32_t lTargetBlockX{ lTargetBlockIndex % lLayerWidthBlock };
            const std::uint32_t lTargetBlockY{ lTargetBlockIndex / lLayerWidthBlock };

            // Initialize the head to the first byte of the current block to draw.
            std::uint32_t lHeadIndex{ lLayerWidthBlock * lTargetBlockY * 16 + lTargetBlockX * 4 };

            std::uint64_t lLookUpIndices{};
            for (std::size_t lIndex{}; lIndex < 6ULL; lIndex++)
            {
                lLookUpIndices |= (static_cast<std::uint64_t>(lBlock.Indices[lIndex]) << (8 * lIndex));
            }
            for (std::uint32_t lTexelIndex{}; lTexelIndex < 16; lTexelIndex++)
            {
                // Move the head to the next row.
                if (lTexelIndex % 4 == 0 && lTexelIndex != 0)
                {
                    lHeadIndex += 4 * (lLayerWidthBlock - 1);
                }

                std::uint32_t lLookUpIndex = lLookUpIndices & 0x7;
                pTargetLayer.Texels[lHeadIndex++] = lColors[lLookUpIndex];
                lLookUpIndices >>= 3;
            }
        }
    }
    return true;
}

} // anonymous namespace

bool SLDTexture::LoadFromFile(
    const std::filesystem::path& pFilepath,
    const LoadOptions& pLoadOptions)
{
    Clear();
    if (std::ifstream lFile{ pFilepath, std::ios::binary })
    {
        SLDTextureHeader lHeader{};
        if (!(lFile >> lHeader))
        {
            return false;
        }

        mFrameCount = lHeader.Frames;

        std::vector<SLDCommand> lCommands;
        std::vector<SLDBC1Block> lBC1Blocks;
        std::vector<SLDBC4Block> lBC4Blocks;

        const std::uint32_t lDirectionFrames{ lHeader.Frames / pLoadOptions.Directions };
        for (std::uint16_t lFrameIndex{}; lFrameIndex < lHeader.Frames; lFrameIndex++)
        {
            std::uint32_t lSourceFrameIndex{};
            if (pLoadOptions.CopyMode == SLDTexture::LoadOptions::CopyModeOptions::FirstFrameInAnimation)
            {
                lSourceFrameIndex = (lFrameIndex / lDirectionFrames) * lDirectionFrames;
            }
            else
            {
                lSourceFrameIndex = (lFrameIndex == 0) ? lFrameIndex : lFrameIndex - 1;
            }

            SLDFrameHeader lFrameHeader{};
            if (!(lFile >> lFrameHeader))
            {
                return false;
            }

            std::uint16_t lColorLayerOffsetX{};
            std::uint16_t lColorLayerOffsetY{};
            std::uint16_t lColorLayerWidth{};
            std::uint16_t lColorLayerHeight{};
            std::uint32_t lColorLayerSize{};
            SLDLayerHeader lLayerHeader{};

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameColorLayerBit)
            {
                SLDLayerHeaderColorDetails lColorLayerHeader{};
                if (!(lFile >> lLayerHeader >> lColorLayerHeader))
                {
                    return false;
                }

                lColorLayerOffsetX = lColorLayerHeader.OffsetX1;
                lColorLayerOffsetY = lColorLayerHeader.OffsetY1;
                lColorLayerWidth = lColorLayerHeader.OffsetX2 - lColorLayerHeader.OffsetX1;
                lColorLayerHeight = lColorLayerHeader.OffsetY2 - lColorLayerHeader.OffsetY1;
                lColorLayerSize = static_cast<std::uint32_t>(lColorLayerWidth) * lColorLayerHeight;

                // Check that the layer dimensions are consistent with BC1 encoding.
                if ((lColorLayerWidth % 4) != 0 || (lColorLayerHeight % 4) != 0)
                {
                    return false;
                }

                SLDCommandArrayHeader lCommandArrayHeader{};
                if (!(lFile >> lCommandArrayHeader))
                {
                    return false;
                }

                std::uint32_t lCommandDrawCount{};
                std::uint32_t lCommandSkipCount{};
                lCommands.resize(lCommandArrayHeader.Length);
                for (std::uint16_t lIndex{}; lIndex < lCommandArrayHeader.Length; lIndex++)
                {
                    if (!(lFile >> lCommands[lIndex]))
                    {
                        return false;
                    }
                    lCommandDrawCount += lCommands[lIndex].DrawCount;
                    lCommandSkipCount += lCommands[lIndex].SkipCount;
                }

                // Check that the layer dimensions are consistent with the command count.
                if ((lColorLayerSize / 16) != (lCommandDrawCount + lCommandSkipCount))
                {
                    return false;
                }

                lBC1Blocks.resize(lCommandDrawCount);
                for (std::uint16_t lIndex{}; lIndex < lCommandDrawCount; lIndex++)
                {
                    if (!(lFile >> lBC1Blocks[lIndex]))
                    {
                        return false;
                    }
                }

                const std::uint32_t lColorLayerSizeInBytes{ lColorLayerSize * 4 };
                mLayerInfo[std::make_pair(lFrameIndex, SLDTexture::LayerType::Color)] = SLDTexture::LayerInfo{
                    .Offset{ mTexels.size() },
                    .Size{ lColorLayerSizeInBytes },
                    .HotspotX{ lFrameHeader.HotspotX },
                    .HotspotY{ lFrameHeader.HotspotY },
                    .OffsetX{ lColorLayerOffsetX },
                    .OffsetY{ lColorLayerOffsetY },
                    .Width{ lColorLayerWidth },
                    .Height{ lColorLayerHeight }
                };
                mTexels.resize(mTexels.size() + lColorLayerSizeInBytes, 0);

                if (lColorLayerSizeInBytes)
                {
                    const bool lCopyLayer{ static_cast<bool>(lColorLayerHeader.Hints & SLDColorLayerHeaderHint::SLDColorLayerCopySourceLayerBit) };
                    SLDTexture::Layer<true> lSourceLayer{ GetLayer(lSourceFrameIndex, SLDTexture::LayerType::Color) };
                    SLDTexture::Layer<true> lTargetLayer{ GetLayer(lFrameIndex, SLDTexture::LayerType::Color) };
                    if (!DecodeBC1Layer(lTargetLayer, lSourceLayer, lCommands, lBC1Blocks, lCopyLayer))
                    {
                        return false;
                    }
                }

                if (!lFile.ignore(lLayerHeader.Padding))
                {
                    return false;
                }
            }
            else if (lFrameHeader.Layers)
            {
                // The color layer is mandatory, unless the frame is empty.
                return false;
            }
            else
            {
                // Ignore empty frame.
                continue;
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameShadowLayerBit)
            {
                SLDLayerHeaderShadowDetails lShadowLayerHeader{};
                if (!(lFile >> lLayerHeader >> lShadowLayerHeader))
                {
                    return false;
                }

                std::uint16_t lShadowLayerOffsetX{ lShadowLayerHeader.OffsetX1 };
                std::uint16_t lShadowLayerOffsetY{ lShadowLayerHeader.OffsetY1 };
                std::uint16_t lShadowLayerWidth{ static_cast<std::uint16_t>(lShadowLayerHeader.OffsetX2 - lShadowLayerHeader.OffsetX1) };
                std::uint16_t lShadowLayerHeight{ static_cast<std::uint16_t>(lShadowLayerHeader.OffsetY2 - lShadowLayerHeader.OffsetY1) };
                std::uint32_t lShadowLayerSize{ static_cast<std::uint32_t>(lShadowLayerWidth) * lShadowLayerHeight };

                // Check that the layer dimensions are consistent with BC4 encoding.
                if ((lShadowLayerWidth % 4) != 0 || (lShadowLayerHeight % 4) != 0)
                {
                    return false;
                }

                SLDCommandArrayHeader lCommandArrayHeader{};
                if (!(lFile >> lCommandArrayHeader))
                {
                    return false;
                }

                std::uint32_t lCommandDrawCount{};
                std::uint32_t lCommandSkipCount{};
                lCommands.resize(lCommandArrayHeader.Length);
                for (std::uint16_t lIndex{}; lIndex < lCommandArrayHeader.Length; lIndex++)
                {
                    if (!(lFile >> lCommands[lIndex]))
                    {
                        return false;
                    }
                    lCommandDrawCount += lCommands[lIndex].DrawCount;
                    lCommandSkipCount += lCommands[lIndex].SkipCount;
                }

                // Check that the layer dimensions are consistent with the command count.
                if ((lShadowLayerSize / 16) != (lCommandDrawCount + lCommandSkipCount))
                {
                    return false;
                }

                lBC4Blocks.resize(lCommandDrawCount);
                for (std::uint16_t lIndex{}; lIndex < lCommandDrawCount; lIndex++)
                {
                    if (!(lFile >> lBC4Blocks[lIndex]))
                    {
                        return false;
                    }
                }

                const std::uint32_t lShadowLayerSizeInBytes{ lShadowLayerSize };
                mLayerInfo[std::make_pair(lFrameIndex, SLDTexture::LayerType::Shadow)] = SLDTexture::LayerInfo{
                    .Offset{ mTexels.size() },
                    .Size{ lShadowLayerSizeInBytes },
                    .HotspotX{ lFrameHeader.HotspotX },
                    .HotspotY{ lFrameHeader.HotspotY },
                    .OffsetX{ lShadowLayerOffsetX },
                    .OffsetY{ lShadowLayerOffsetY },
                    .Width{ lShadowLayerWidth },
                    .Height{ lShadowLayerHeight }
                };
                mTexels.resize(mTexels.size() + lShadowLayerSizeInBytes, 0);

                if (lShadowLayerSizeInBytes)
                {
                    const bool lCopyLayer{ static_cast<bool>(lShadowLayerHeader.Hints & SLDShadowLayerHeaderHint::SLDShadowLayerCopySourceLayerBit) };
                    SLDTexture::Layer<true> lSourceLayer{ GetLayer(lSourceFrameIndex, SLDTexture::LayerType::Shadow) };
                    SLDTexture::Layer<true> lTargetLayer{ GetLayer(lFrameIndex, SLDTexture::LayerType::Shadow) };
                    if (!DecodeBC4Layer(lTargetLayer, lSourceLayer, lCommands, lBC4Blocks, lCopyLayer))
                    {
                        return false;
                    }
                }

                if (!lFile.ignore(lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameUnknownLayer1Bit)
            {
                if (!(lFile >> lLayerHeader))
                {
                    return false;
                }
                if (!lFile.ignore(static_cast<std::uint64_t>(lLayerHeader.Length) - sizeof(std::uint32_t) + lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameDamageMaskLayerBit)
            {
                SLDLayerHeaderDamageMaskDetails lDamageMaskLayerHeader{};
                if (!(lFile >> lLayerHeader >> lDamageMaskLayerHeader))
                {
                    return false;
                }

                SLDCommandArrayHeader lCommandArrayHeader{};
                if (!(lFile >> lCommandArrayHeader))
                {
                    return false;
                }

                std::uint32_t lCommandDrawCount{};
                std::uint32_t lCommandSkipCount{};
                lCommands.resize(lCommandArrayHeader.Length);
                for (std::uint16_t lIndex{}; lIndex < lCommandArrayHeader.Length; lIndex++)
                {
                    if (!(lFile >> lCommands[lIndex]))
                    {
                        return false;
                    }
                    lCommandDrawCount += lCommands[lIndex].DrawCount;
                    lCommandSkipCount += lCommands[lIndex].SkipCount;
                }

                // Check that the layer dimensions are consistent with the command count.
                if ((lColorLayerSize / 16) != (lCommandDrawCount + lCommandSkipCount))
                {
                    return false;
                }

                lBC1Blocks.resize(lCommandDrawCount);
                for (std::uint16_t lIndex{}; lIndex < lCommandDrawCount; lIndex++)
                {
                    if (!(lFile >> lBC1Blocks[lIndex]))
                    {
                        return false;
                    }
                }

                const std::uint32_t lDamageColorMaskLayerSizeInBytes{ lColorLayerSize * 4 };
                mLayerInfo[std::make_pair(lFrameIndex, SLDTexture::LayerType::DamageColorMask)] = SLDTexture::LayerInfo{
                    .Offset{ mTexels.size() },
                    .Size{ lDamageColorMaskLayerSizeInBytes },
                    .HotspotX{ lFrameHeader.HotspotX },
                    .HotspotY{ lFrameHeader.HotspotY },
                    .OffsetX{ lColorLayerOffsetX },
                    .OffsetY{ lColorLayerOffsetY },
                    .Width{ lColorLayerWidth },
                    .Height{ lColorLayerHeight }
                };
                mTexels.resize(mTexels.size() + lDamageColorMaskLayerSizeInBytes, 0);

                if (lDamageColorMaskLayerSizeInBytes)
                {
                    const bool lCopyLayer{ static_cast<bool>(lDamageMaskLayerHeader.Hints & SLDDamageMaskLayerHeaderHint::SLDDamageMaskLayerCopySourceLayerBit) };
                    SLDTexture::Layer<true> lSourceLayer{ GetLayer(lSourceFrameIndex, SLDTexture::LayerType::DamageColorMask) };
                    SLDTexture::Layer<true> lTargetLayer{ GetLayer(lFrameIndex, SLDTexture::LayerType::DamageColorMask) };
                    if (!DecodeBC1Layer(lTargetLayer, lSourceLayer, lCommands, lBC1Blocks, lCopyLayer))
                    {
                        return false;
                    }
                }

                if (!lFile.ignore(lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFramePlayerMaskLayerBit)
            {
                SLDLayerHeaderPlayerMaskDetails lPlayerMaskLayerHeader{};
                if (!(lFile >> lLayerHeader >> lPlayerMaskLayerHeader))
                {
                    return false;
                }

                SLDCommandArrayHeader lCommandArrayHeader{};
                if (!(lFile >> lCommandArrayHeader))
                {
                    return false;
                }

                std::uint32_t lCommandDrawCount{};
                std::uint32_t lCommandSkipCount{};
                lCommands.resize(lCommandArrayHeader.Length);
                for (std::uint16_t lIndex{}; lIndex < lCommandArrayHeader.Length; lIndex++)
                {
                    if (!(lFile >> lCommands[lIndex]))
                    {
                        return false;
                    }
                    lCommandDrawCount += lCommands[lIndex].DrawCount;
                    lCommandSkipCount += lCommands[lIndex].SkipCount;
                }

                // Check that the layer dimensions are consistent with the command count.
                if ((lColorLayerSize / 16) != (lCommandDrawCount + lCommandSkipCount))
                {
                    return false;
                }

                lBC4Blocks.resize(lCommandDrawCount);
                for (std::uint16_t lIndex{}; lIndex < lCommandDrawCount; lIndex++)
                {
                    if (!(lFile >> lBC4Blocks[lIndex]))
                    {
                        return false;
                    }
                }

                const std::uint32_t lPlayerColorMaskLayerSizeInBytes{ lColorLayerSize };
                mLayerInfo[std::make_pair(lFrameIndex, SLDTexture::LayerType::PlayerColorMask)] = SLDTexture::LayerInfo{
                    .Offset{ mTexels.size() },
                    .Size{ lPlayerColorMaskLayerSizeInBytes },
                    .HotspotX{ lFrameHeader.HotspotX },
                    .HotspotY{ lFrameHeader.HotspotY },
                    .OffsetX{ lColorLayerOffsetX },
                    .OffsetY{ lColorLayerOffsetY },
                    .Width{ lColorLayerWidth },
                    .Height{ lColorLayerHeight }
                };
                mTexels.resize(mTexels.size() + lPlayerColorMaskLayerSizeInBytes, 0);

                if (lPlayerColorMaskLayerSizeInBytes)
                {
                    const bool lCopyLayer{ static_cast<bool>(lPlayerMaskLayerHeader.Hints & SLDPlayerMaskLayerHeaderHint::SLDPlayerMaskLayerCopySourceLayerBit) };
                    SLDTexture::Layer<true> lSourceLayer{ GetLayer(lSourceFrameIndex, SLDTexture::LayerType::PlayerColorMask) };
                    SLDTexture::Layer<true> lTargetLayer{ GetLayer(lFrameIndex, SLDTexture::LayerType::PlayerColorMask) };
                    if (!DecodeBC4Layer(lTargetLayer, lSourceLayer, lCommands, lBC4Blocks, lCopyLayer))
                    {
                        return false;
                    }
                }

                if (!lFile.ignore(lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameUnknownLayer2Bit)
            {
                if (!(lFile >> lLayerHeader))
                {
                    return false;
                }
                if (!lFile.ignore(static_cast<std::uint64_t>(lLayerHeader.Length) - sizeof(std::uint32_t) + lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameUnknownLayer3Bit)
            {
                if (!(lFile >> lLayerHeader))
                {
                    return false;
                }
                if (!lFile.ignore(static_cast<std::uint64_t>(lLayerHeader.Length) - sizeof(std::uint32_t) + lLayerHeader.Padding))
                {
                    return false;
                }
            }

            if (lFrameHeader.Layers & SLDLayerType::SLDFrameUnknownLayer4Bit)
            {
                if (!(lFile >> lLayerHeader))
                {
                    return false;
                }
                if (!lFile.ignore(static_cast<std::uint64_t>(lLayerHeader.Length) - sizeof(std::uint32_t) + lLayerHeader.Padding))
                {
                    return false;
                }
            }
        }
        return lFile.peek() == EOF;
    }
    return false;
}

void SLDTexture::Clear()
{
    mFrameCount = 0ULL;
    mLayerInfo.clear();
    mTexels.clear();
}

SLDTexture::Layer<false> SLDTexture::GetLayer(
    std::size_t pFrameIndex,
    SLDTexture::LayerType pType) const
{
    if (auto lIt{ mLayerInfo.find(std::make_pair(pFrameIndex, pType)) }; lIt != mLayerInfo.cend())
    {
        const char* lLayerTexels{ mTexels.data() + lIt->second.Offset };
        return SLDTexture::Layer<false>{
            .Texels{ lLayerTexels },
            .Size{ lIt->second.Size },
            .HotspotX{ lIt->second.HotspotX },
            .HotspotY{ lIt->second.HotspotY },
            .OffsetX{ lIt->second.OffsetX },
            .OffsetY{ lIt->second.OffsetY },
            .Width{ lIt->second.Width },
            .Height{ lIt->second.Height }
        };
    }
    return SLDTexture::Layer<false>{};
}

SLDTexture::Layer<true> SLDTexture::GetLayer(
    std::size_t pFrameIndex,
    SLDTexture::LayerType pType)
{
    if (auto lIt{ mLayerInfo.find(std::make_pair(pFrameIndex, pType)) }; lIt != mLayerInfo.cend())
    {
        char* lLayerTexels{ mTexels.data() + lIt->second.Offset };
        return SLDTexture::Layer<true>{
            .Texels{ lLayerTexels },
            .Size{ lIt->second.Size },
            .HotspotX{ lIt->second.HotspotX },
            .HotspotY{ lIt->second.HotspotY },
            .OffsetX{ lIt->second.OffsetX },
            .OffsetY{ lIt->second.OffsetY },
            .Width{ lIt->second.Width },
            .Height{ lIt->second.Height }
        };
    }
    return SLDTexture::Layer<true>{};
}

std::size_t SLDTexture::GetFrameCount() const
{
    return mFrameCount;
}

} // AoE2DE::Core::Media