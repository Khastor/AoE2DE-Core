#ifndef AOE2DE_CORE_MEDIA_SLD_TEXTURE_HPP_INCLUDED
#define AOE2DE_CORE_MEDIA_SLD_TEXTURE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// SLDTexture Interface                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstddef>
#include <filesystem>
#include <map>
#include <type_traits>
#include <utility>

namespace AoE2DE::Core::Media
{

/**
 * @brief SLD texture.
 */
class SLDTexture
{
public:
    /**
     * @brief SLD texture loading options.
     */
    struct LoadOptions
    {
        /**
         * @brief The number of animation directions in the SLD file.
         */
        std::uint32_t Directions;

        /**
         * @brief Copy mode for skipped blocks in layers where the copy hint is enabled.
         */
        enum class CopyModeOptions
        {
            /**
             * @brief Skipped blocks are copied from the first frame of the current animation direction.
             */
            FirstFrameInAnimation,

            /**
             * @brief Skipped blocks are copied from the previous frame.
             */
            PreviousFrame
        };

        /**
         * @brief Copy mode for skipped blocks in layers where the copy hint is enabled.
         */
        CopyModeOptions CopyMode;
    };

    /**
     * @brief Load the SLD texture from a file path.
     *
     * @param pFilepath The path to the SLD texture file.
     * @param pLoadOptions The SLD texture loading options.
     * @return True if the SLD texture was successfully loaded and decoded.
     */
    bool LoadFromFile(
        const std::filesystem::path& pFilepath,
        const LoadOptions& pLoadOptions);

    /**
     * @brief Clear the SLD texture.
     */
    void Clear();

    /**
     * @brief SLD texture layer type.
     */
    enum class LayerType
    {
        /**
         * @brief The main texture of the game entity.
         */
        Color,

        /**
         * @brief An overlay for the main texture that stores player color indices for each texel.
         */
        PlayerColorMask,

        /**
         * @brief An overlay for the main texture that stores modifiers to compute damage darkening effects.
         */
        DamageColorMask,

        /**
         * @brief The shadows of the game entity.
         */
        Shadow
    };

    /**
     * @brief SLD texture layer.
     *
     * @tparam tWritable True if the texel data is writable, false if it is read-only.
     */
    template<bool tWritable = false>
    struct Layer
    {
        /**
         * @brief The texel data.
         */
        std::conditional_t<tWritable, char*, const char*> Texels;

        /**
         * @brief The size of the texel data, in bytes.
         */
        std::size_t Size;

        /**
         * @brief The horizontal offset of the frame canvas hotspot.
         */
        std::int16_t HotspotX;

        /**
         * @brief The vertical offset of the frame canvas hotspot.
         */
        std::int16_t HotspotY;

        /**
         * @brief The horizontal offset of the layer within its frame canvas.
         */
        std::uint16_t OffsetX;

        /**
         * @brief The vertical offset of the layer within its frame canvas.
         */
        std::uint16_t OffsetY;

        /**
         * @brief The witdh of the layer, in texels.
         */
        std::uint16_t Width;

        /**
         * @brief The height of the layer, in texels.
         */
        std::uint16_t Height;
    };

    /**
     * @brief Get a read-only SLD texture layer.
     *
     * @param pFrameIndex The index of the texture layer's frame.
     * @param pType The layer to get from the requested frame.
     * @return The layer matching the requested frame index and type if it exists, otherwise a null layer.
     */
    SLDTexture::Layer<false> GetLayer(
        std::size_t pFrameIndex,
        SLDTexture::LayerType pType) const;

    /**
     * @brief Get a writable SLD texture layer.
     *
     * @param pFrameIndex The index of the texture layer's frame.
     * @param pType The layer to get from the requested frame.
     * @return The layer matching the requested frame index and type if it exists, otherwise a null layer.
     */
    SLDTexture::Layer<true> GetLayer(
        std::size_t pFrameIndex,
        SLDTexture::LayerType pType);

    /**
     * @brief Get the number of frames in the SLD texture.
     *
     * @return The number of frames in the SLD texture.
     */
    std::size_t GetFrameCount() const;

private:
    /**
     * @brief SLD texture layer metadata.
     */
    struct LayerInfo
    {
        /**
         * @brief The offset of the texel data, in bytes.
         */
        std::size_t Offset;

        /**
         * @brief The size of the texel data, in bytes.
         */
        std::size_t Size;

        /**
         * @brief The horizontal offset of the frame canvas hotspot.
         */
        std::int16_t HotspotX;

        /**
         * @brief The vertical offset of the frame canvas hotspot.
         */
        std::int16_t HotspotY;

        /**
         * @brief The horizontal offset of the layer within its frame canvas.
         */
        std::uint16_t OffsetX;

        /**
         * @brief The vertical offset of the layer within its frame canvas.
         */
        std::uint16_t OffsetY;

        /**
         * @brief The witdh of the layer, in texels.
         */
        std::uint16_t Width;

        /**
         * @brief The height of the layer, in texels.
         */
        std::uint16_t Height;
    };

    /**
     * @brief The SLD texture frame count.
     */
    std::size_t mFrameCount;

    /**
     * @brief The SLD texture layer metadata.
     *
     * Map a frame index and a layer type to the corresponding layer metadata.
     */
    std::map<std::pair<std::size_t, SLDTexture::LayerType>, SLDTexture::LayerInfo> mLayerInfo;

    /**
     * @brief The SLD texture texel data.
     */
    std::vector<char> mTexels;
};

} // AoE2DE::Core::Media

#endif // AOE2DE_CORE_MEDIA_SLD_TEXTURE_HPP_INCLUDED