# Age of Empires II Definitive Edition Core

Core library for using Age of Empires II Definitive Edition assets.

## Dependencies

* [stb_image_write](https://github.com/nothings/stb)

## Build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```